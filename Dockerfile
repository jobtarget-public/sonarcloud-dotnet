FROM mcr.microsoft.com/dotnet/aspnet:6.0 AS base

WORKDIR /app
EXPOSE 80

FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
WORKDIR /src
COPY ["PrimeService/PrimeService.csproj", "PrimeService/"]
RUN dotnet restore "PrimeService/PrimeService.csproj"
COPY . .
WORKDIR "/src/PrimeService"
RUN dotnet build "PrimeService.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "PrimeService.csproj" -c Release -o /app/publish /p:UseAppHost=false

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "PrimeService.dll"]
